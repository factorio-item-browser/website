import { atom } from "recoil";
import { Entity } from "../model/entity";

export const sidebarLastViewedState = atom<Entity[]>({
    key: "sidebar.last-viewed",
    default: [],
});
