import { atom } from "recoil";
import { RecipeMode } from "../model/entity";
import { repository } from "../model/repository";

type Options = {
    locale: string;
    recipeMode: RecipeMode;
};

export const optionsState = atom<Options>({
    key: "options",
    default: {
        locale: "en",
        recipeMode: RecipeMode.Normal,
    },
    effects: [
        ({ onSet }) => {
            onSet((newOptions) => {
                repository.options.locale = newOptions.locale;
                repository.options.recipeMode = newOptions.recipeMode;
            });
        },
    ],
});
