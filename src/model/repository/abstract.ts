import { Table } from "dexie";
import { Options } from "../entity";
import { arrayToObject, filterNotEmpty } from "./helper";

type MinimalEntity = {
    key: string;
};

export type Translations = Record<string, string>;

/**
 * The abstract class of the repositories with some common logic.
 */
export abstract class AbstractRepository<TEntity extends MinimalEntity, TDatabaseEntity> {
    public constructor(protected readonly table: Table<TDatabaseEntity, string>, protected readonly options: Options) {}

    /**
     * Maps the database entity to a data entity.
     */
    protected abstract map(entity: TDatabaseEntity): Promise<TEntity>;

    /**
     * Maps an array of database entities to their data entities.
     */
    protected async mapArray(entities: TDatabaseEntity[]): Promise<TEntity[]> {
        const result: TEntity[] = [];
        for (const entity of entities) {
            result.push(await this.map(entity));
        }
        return result;
    }

    /**
     * Finds the entity of the provided key. Returns null if the key is not known.
     */
    public async findByKey(key: string): Promise<TEntity | null> {
        const entity = await this.table.get(key);
        return entity ? this.map(entity) : null;
    }

    /**
     * Finds multiple entities by their keys. The returned record will have the entities assigned by their keys. If
     * a key is not known, it will not appear in the returned record.
     */
    public async findByKeys(keys: string[]): Promise<Record<string, TEntity>> {
        const entities = await this.table.where("key").startsWithAnyOf(keys).toArray();
        const mappedEntities = await this.mapArray(filterNotEmpty(entities));
        return arrayToObject(mappedEntities);
    }
}

/**
 * The abstract class of the importers.
 */
export abstract class AbstractImporter<TExportEntity, TDatabaseEntity> {
    private entities: TDatabaseEntity[] = [];

    /**
     * Adds an entity to be imported.
     */
    public add(entity: TExportEntity): void {
        const mappedEntity = this.map(entity);
        if (mappedEntity !== null) {
            this.entities.push(mappedEntity);
        }
    }

    /**
     * Maps the provided export entity to its database representation, if valid.
     */
    protected abstract map(entity: TExportEntity): TDatabaseEntity | null;

    /**
     * Persists the previously added entities to the database.
     */
    public async persist(table: Table<TDatabaseEntity, string>): Promise<void> {
        if (this.entities.length === 0) {
            return;
        }

        await table.bulkPut(this.entities);
        this.entities = [];
    }
}
