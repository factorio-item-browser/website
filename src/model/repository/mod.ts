import { ExportMod } from "../../export/data";
import { Mod, ModType } from "../entity";
import { AbstractImporter, AbstractRepository, Translations } from "./abstract";
import { buildKey, parseKey, translate } from "./helper";

export type DatabaseMod = {
    key: string;
    labels: Translations;
    descriptions: Translations;
    author: string;
    version: string;
    iconId: string;
};

export class ModRepository extends AbstractRepository<Mod, DatabaseMod> {
    protected async map(mod: DatabaseMod): Promise<Mod> {
        const [type, name] = parseKey(mod.key);
        return {
            key: mod.key,
            type: type as ModType,
            name: name,
            label: translate(mod.labels, this.options.locale, mod.key),
            description: translate(mod.descriptions, this.options.locale, ""),
            author: mod.author,
            version: mod.version,
            iconId: mod.iconId,
        };
    }

    /**
     * Finds and returns all mods, sorted by their internal name.
     */
    public async findAll(): Promise<Mod[]> {
        const mods = await this.table.toArray();
        const mappedMods = await this.mapArray(mods);
        mappedMods.sort((left: Mod, right: Mod) => left.name.localeCompare(right.name));
        return mappedMods;
    }
}

export class ModImporter extends AbstractImporter<ExportMod, DatabaseMod> {
    protected map(mod: ExportMod): DatabaseMod | null {
        const key = buildKey(mod.type, mod.name);
        return {
            key: key,
            labels: mod.labels,
            descriptions: mod.descriptions,
            author: mod.author,
            version: mod.version,
            iconId: mod.icon,
        };
    }
}
