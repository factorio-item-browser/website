import { Translations } from "./abstract";

/**
 * Transforms the provided array of entities to an object, using the entity keys as keys for the object.
 */
export function arrayToObject<T extends { key: string }>(entities: T[]): Record<string, T> {
    const result: Record<string, T> = {};
    for (const entity of entities) {
        result[entity.key] = entity;
    }
    return result;
}

/**
 * Filters any empty values from the provided array, returning only the actually not-empty values.
 */
export function filterNotEmpty<T>(entities: (T | null | undefined)[]): T[] {
    return entities.filter((entity): entity is T => {
        return entity !== undefined && entity !== null;
    });
}

export function filterBlacklisted<T extends { name: string }>(entities: T[], blacklist: string[]): T[] {
    const escapeRegex = (str: string): string => str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
    const regExps: RegExp[] = [];
    for (const rule of blacklist) {
        regExps.push(new RegExp("^" + rule.split("*").map(escapeRegex).join(".*") + "$"));
    }

    return entities.filter((entity): boolean => {
        for (const regexp of regExps) {
            if (regexp.test(entity.name)) {
                return false;
            }
        }
        return true;
    });
}

/**
 * Parses the provided key to retrieve the type and name from it.
 */
export function parseKey(key: string, withMode?: boolean): [string, string, string] {
    const parts = key.split(".");
    const type = parts.shift() || "";
    const mode = withMode ? parts.pop() || "" : "";
    const name = parts.join(".");

    return [type, name, mode];
}

/**
 * Builds the key using the provided type and name, and optionally its mode.
 */
export function buildKey(type: string, name: string, mode?: string): string {
    if (mode) {
        return `${type}.${name}.${mode}`;
    }

    return `${type}.${name}`;
}

/**
 * Extracts the correct translation from the provided ones, preferring the provided locale.
 */
export function translate(translations: Translations, locale: string, fallback: string): string {
    return translations[locale] || translations["en"] || fallback;
}
