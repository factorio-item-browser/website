import { Translations } from "./abstract";
import { arrayToObject, buildKey, filterNotEmpty, parseKey, translate } from "./helper";

describe("arrayToObject", () => {
    const entity1 = { key: "foo" };
    const entity2 = { key: "bar" };
    const entity3 = { key: "baz" };

    test.each([
        [
            "default",
            [entity1, entity2, entity3],
            {
                foo: entity1,
                bar: entity2,
                baz: entity3,
            },
        ],
        ["empty array", [], {}],
    ])("%s", (_, entities, expectedResult) => {
        const result = arrayToObject(entities);

        expect(result).toEqual(expectedResult);
    });
});

describe("filterNotEmpty", () => {
    test.each([
        ["no filter", ["foo", "bar", "baz"], ["foo", "bar", "baz"]],
        ["with filter", ["foo", null, "bar", undefined, "baz"], ["foo", "bar", "baz"]],
        ["empty array", [], []],
    ])("%s", (_, entities, expectedResult) => {
        const result = filterNotEmpty(entities);

        expect(result).toEqual(expectedResult);
    });
});

describe("parseKey", () => {
    test.each([
        ["simple", "foo.bar", false, "foo", "bar", ""],
        ["simple with mode", "foo.bar.baz", true, "foo", "bar", "baz"],
        ["dot in name", "foo.bar.baz", false, "foo", "bar.baz", ""],
        ["dot in name with mode", "foo.bar.baz.wuppdi", true, "foo", "bar.baz", "wuppdi"],
        ["empty", "", false, "", "", ""],
        ["empty with mode", "", true, "", "", ""],
    ])("%s", (_, key, withMode, expectedType, expectedName, expectedMode) => {
        const [type, name, mode] = parseKey(key, withMode);

        expect(type).toEqual(expectedType);
        expect(name).toEqual(expectedName);
        expect(mode).toEqual(expectedMode);
    });
});

describe("buildKey", () => {
    test.each([
        ["simple", "foo", "bar", undefined, "foo.bar"],
        ["simple with mode", "foo", "bar", "baz", "foo.bar.baz"],
        ["with dot", "foo", "bar.baz", undefined, "foo.bar.baz"],
        ["with dot and mode", "foo", "bar.baz", "wuppdi", "foo.bar.baz.wuppdi"],
    ])("%s", (_, type, name, mode, expectedResult) => {
        const result = buildKey(type, name, mode);

        expect(result).toEqual(expectedResult);
    });
});

describe("translate", () => {
    const translations1: Translations = {
        en: "foo",
        de: "bar",
    };
    const translations2: Translations = {
        de: "bar",
    };

    test.each([
        ["matched locale", translations1, "de", "fail", "bar"],
        ["fallback English", translations1, "fr", "fail", "foo"],
        ["missing English", translations2, "fr", "foo", "foo"],
    ])("%s", (_, translations, locale, fallback, expectedResult) => {
        const result = translate(translations, locale, fallback);

        expect(result).toEqual(expectedResult);
    });
});
