import { ExportItem } from "../../export/data";
import { Item, ItemType } from "../entity";
import { AbstractImporter, AbstractRepository, Translations } from "./abstract";
import { buildKey, filterBlacklisted, parseKey, translate } from "./helper";

export type DatabaseItem = {
    key: string;
    labels: Translations;
    descriptions: Translations;
    iconId: string;
};

export class ItemRepository extends AbstractRepository<Item, DatabaseItem> {
    protected async map(item: DatabaseItem): Promise<Item> {
        const [type, name] = parseKey(item.key);
        return {
            key: item.key,
            type: type as ItemType,
            name: name,
            label: translate(item.labels, this.options.locale, item.key),
            description: translate(item.descriptions, this.options.locale, ""),
            iconId: item.iconId,
        };
    }

    /**
     * Finds and returns all items, sorted by their internal name.
     */
    public async findAll(): Promise<Item[]> {
        const items = await this.table.toArray();
        const mappedItems = await this.mapArray(items);
        const filteredItems = filterBlacklisted(mappedItems, this.options.filterItems);
        filteredItems.sort((left: Item, right: Item) => left.name.localeCompare(right.name));
        return filteredItems;
    }

    /**
     * Finds and returns the provided number of random items.
     */
    public async findRandom(numberOfResults: number): Promise<Item[]> {
        const numberOfItems = await this.table.count();
        const indexes = this.selectRandomNumbers(numberOfItems, numberOfResults).sort((a, b) => a - b);

        const items: DatabaseItem[] = [];
        let currentIndex = 0;
        await this.table
            .toCollection()
            .until((item: DatabaseItem): boolean => {
                if (indexes.length === 0) {
                    return true;
                }

                if (currentIndex === indexes[0]) {
                    items.push(item);
                    indexes.shift();
                }

                ++currentIndex;
                return false;
            })
            .count();

        return this.mapArray(this.shuffle(items));
    }

    private selectRandomNumbers(max: number, count: number): number[] {
        if (count > max) {
            count = max;
        }

        const result = [];
        while (result.length < count) {
            const idx = Math.floor(Math.random() * max);
            if (result.indexOf(idx) === -1) {
                result.push(idx);
            }
        }
        return result;
    }

    private shuffle(items: DatabaseItem[]): DatabaseItem[] {
        for (let i = items.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [items[i], items[j]] = [items[j], items[i]];
        }
        return items;
    }
}

export class ItemImporter extends AbstractImporter<ExportItem, DatabaseItem> {
    protected map(item: ExportItem): DatabaseItem | null {
        const key = buildKey(item.type, item.name);
        return {
            key: key,
            labels: item.labels,
            descriptions: item.descriptions,
            iconId: item.icon,
        };
    }
}
