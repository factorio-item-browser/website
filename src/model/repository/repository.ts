import Dexie, { Table } from "dexie";
import { Exporter } from "../../export/exporter";
import { Options, RecipeMode } from "../entity";
import { DatabaseItem, ItemImporter, ItemRepository } from "./item";
import { DatabaseMod, ModImporter, ModRepository } from "./mod";
import { DatabaseRecipe, RecipeImporter, RecipeRepository } from "./recipe";

class Database extends Dexie {
    public items!: Table<DatabaseItem, string>;
    public mods!: Table<DatabaseMod, string>;
    public recipes!: Table<DatabaseRecipe, string>;

    public constructor(combinationId: string) {
        super(combinationId);

        this.version(1).stores({
            items: "key",
            mods: "key",
            recipes: "key,*ingredientKeys,*productKeys",
        });
    }
}

export class Repository {
    private database: Database;
    public readonly options: Options;

    public readonly items: ItemRepository;
    public readonly mods: ModRepository;
    public readonly recipes: RecipeRepository;

    public constructor() {
        this.database = new Database("space-exploration");
        this.options = {
            locale: "en",
            recipeMode: RecipeMode.Normal,
            filterItems: ["textplate-large-*-*", "textplate-small-*-*"],
        };

        this.items = new ItemRepository(this.database.items, this.options);
        this.mods = new ModRepository(this.database.mods, this.options);
        this.recipes = new RecipeRepository(this.database.recipes, this.options, this.items);
    }

    public async import(): Promise<void> {
        console.log("Close database");
        this.database.close();
        console.log("Delete database");
        await this.database.delete();
        console.log("Open database");
        await this.database.open();

        const itemImporter = new ItemImporter();
        const modImporter = new ModImporter();
        const recipeImporter = new RecipeImporter();

        const exporter = new Exporter();
        exporter.onItem = itemImporter.add.bind(itemImporter);
        exporter.onMod = modImporter.add.bind(modImporter);
        exporter.onRecipe = recipeImporter.add.bind(recipeImporter);

        exporter.onEnd = async () => {
            await Promise.all([
                this.database.transaction("rw", this.database.items, async () => {
                    await itemImporter.persist(this.database.items);
                }),
                this.database.transaction("rw", this.database.mods, async () => {
                    await modImporter.persist(this.database.mods);
                }),
                this.database.transaction("rw", this.database.recipes, async () => {
                    await recipeImporter.persist(this.database.recipes);
                }),
            ]);
            console.log("Importing complete.");
        };

        console.log("Starting exporter");
        await exporter.process(this.database.name);
    }
}

export const repository = new Repository();
