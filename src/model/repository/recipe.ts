import Dexie from "dexie";
import { ExportRecipe } from "../../export/data";
import { Item, ItemWithAmount, Options, Recipe, RecipeMode, RecipeType } from "../entity";
import { AbstractImporter, AbstractRepository, Translations } from "./abstract";
import { buildKey, parseKey, translate } from "./helper";
import { ItemRepository } from "./item";

export type DatabaseRecipe = {
    key: string;
    labels: Translations;
    descriptions: Translations;
    category: string;
    time: number;
    ingredientKeys: string[];
    ingredientAmounts: number[];
    productKeys: string[];
    productAmounts: number[];
    iconId: string;
};

export class RecipeRepository extends AbstractRepository<Recipe, DatabaseRecipe> {
    public constructor(
        table: Dexie.Table<DatabaseRecipe, string>,
        options: Options,
        private readonly itemRepository: ItemRepository,
    ) {
        super(table, options);
    }

    protected async map(recipe: DatabaseRecipe, prefetchedItems?: Record<string, Item>): Promise<Recipe> {
        if (!prefetchedItems) {
            prefetchedItems = await this.fetchRecipeItems([recipe]);
        }

        const [type, name, mode] = parseKey(recipe.key, true);
        const key = buildKey(type, name);
        return {
            key: key,
            type: type as RecipeType,
            name: name,
            mode: mode as RecipeMode,
            label: translate(recipe.labels, this.options.locale, key),
            description: translate(recipe.descriptions, this.options.locale, ""),
            category: recipe.category,
            time: recipe.time,
            ingredients: this.createItemsWithAmount(recipe.ingredientKeys, recipe.ingredientAmounts, prefetchedItems),
            products: this.createItemsWithAmount(recipe.productKeys, recipe.productAmounts, prefetchedItems),
            iconId: recipe.iconId,
        };
    }

    protected async mapArray(recipes: DatabaseRecipe[]): Promise<Recipe[]> {
        const prefetchedItems = await this.fetchRecipeItems(recipes);

        const result: Record<string, Recipe> = {};
        for (const recipe of recipes) {
            const mappedRecipe = await this.map(recipe, prefetchedItems);
            const [, , mode] = parseKey(recipe.key, true);

            if (this.options.recipeMode === mode || (mode === RecipeMode.Normal && !result[mappedRecipe.key])) {
                result[mappedRecipe.key] = mappedRecipe;
            }
        }

        return Object.values(result);
    }

    /**
     * Fetches all items (ingredients and products) of the provided recipes from the database.
     */
    private async fetchRecipeItems(recipes: DatabaseRecipe[]): Promise<Record<string, Item>> {
        const itemKeys: string[] = [];
        for (const recipe of recipes) {
            itemKeys.push(...recipe.ingredientKeys, ...recipe.productKeys);
        }

        return await this.itemRepository.findByKeys(itemKeys);
    }

    /**
     * Creates the items with their amounts, combining the provided keys and amounts together.
     */
    private createItemsWithAmount(keys: string[], amounts: number[], items: Record<string, Item>): ItemWithAmount[] {
        const result: ItemWithAmount[] = [];
        for (let i = 0; i < keys.length; ++i) {
            const item = items[keys[i]];
            if (!item || !amounts[i]) {
                continue;
            }

            result.push({
                ...item,
                amount: amounts[i],
            });
        }
        return result;
    }

    /**
     * Finds all recipes with the provided ingredient. The ingredients and products of the recipes will be sorted to
     * prefer the specified key.
     */
    public async findWithIngredient(itemKey: string): Promise<Recipe[]> {
        const databaseRecipes = await this.table.where("ingredientKeys").equals(itemKey).toArray();
        const recipes = await this.mapArray(databaseRecipes);
        for (const recipe of recipes) {
            this.sortIngredientsAndProducts(recipe, itemKey);
        }
        return this.sortRecipes(recipes, (recipe) => recipe.ingredients.length);
    }

    /**
     * Finds all recipes with the provided product. The ingredients and products of the recipes will be sorted to
     * prefer the specified key.
     */
    public async findWithProduct(itemKey: string): Promise<Recipe[]> {
        const databaseRecipes = await this.table.where("productKeys").equals(itemKey).toArray();
        const recipes = await this.mapArray(databaseRecipes);
        for (const recipe of recipes) {
            this.sortIngredientsAndProducts(recipe, itemKey);
        }
        return this.sortRecipes(recipes, (recipe) => recipe.products.length);
    }

    /**
     * Sorts the ingredients and products of the provided recipe, preferring the provided key to be sorted to the front.
     */
    private sortIngredientsAndProducts(recipe: Recipe, preferredKey: string): void {
        const compare = (left: ItemWithAmount, right: ItemWithAmount): number => {
            if (left.key === preferredKey) {
                return -1;
            }
            if (right.key === preferredKey) {
                return 1;
            }
            return 0;
        };

        recipe.ingredients.sort(compare);
        recipe.products.sort(compare);
    }

    private sortRecipes(recipes: Recipe[], itemCounter: (recipe: Recipe) => number): Recipe[] {
        const compare = (left: Recipe, right: Recipe): number => {
            const leftCount = itemCounter(left);
            const rightCount = itemCounter(right);
            if (leftCount === rightCount) {
                return left.key.localeCompare(right.key);
            }
            return leftCount - rightCount;
        };

        recipes.sort(compare);
        return recipes;
    }
}

export class RecipeImporter extends AbstractImporter<ExportRecipe, DatabaseRecipe> {
    protected map(recipe: ExportRecipe): DatabaseRecipe | null {
        const key = buildKey(recipe.type, recipe.name, recipe.mode);
        const result: DatabaseRecipe = {
            key: key,
            labels: recipe.labels,
            descriptions: recipe.descriptions,
            category: recipe.category,
            time: recipe.time,
            ingredientKeys: [],
            ingredientAmounts: [],
            productKeys: [],
            productAmounts: [],
            iconId: recipe.icon,
        };

        for (const ingredient of recipe.ingredients) {
            result.ingredientKeys.push(buildKey(ingredient.type, ingredient.name));
            result.ingredientAmounts.push(ingredient.amount);
        }

        for (const product of recipe.products) {
            result.productKeys.push(buildKey(product.type, product.name));
            result.productAmounts.push(((product.amountMax + product.amountMin) / 2) * product.probability);
        }

        return result;
    }
}
