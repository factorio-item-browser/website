export enum ItemType {
    Item = "item",
    Fluid = "fluid",
    Resource = "resource",
}

export type Item = {
    key: string;
    type: ItemType;
    name: string;
    label: string;
    description: string;
    iconId: string;
};

export type ItemWithAmount = Item & {
    amount: number;
};
