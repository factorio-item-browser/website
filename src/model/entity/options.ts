import { RecipeMode } from "./recipe";

export type Options = {
    locale: string;
    recipeMode: RecipeMode;
    filterItems: string[];
};
