export enum ModType {
    Mod = "mod",
}

export type Mod = {
    key: string;
    type: ModType;
    name: string;
    label: string;
    description: string;
    author: string;
    version: string;
    iconId: string;
};
