import { ItemWithAmount } from "./item";

export enum RecipeType {
    Recipe = "recipe",
    RocketLaunch = "rocket-launch",
    Mining = "mining",
}

export enum RecipeMode {
    Normal = "normal",
    Expensive = "expensive",
}

export type Recipe = {
    key: string;
    type: RecipeType;
    name: string;
    mode: RecipeMode;
    label: string;
    description: string;
    category: string;
    time: number;
    ingredients: ItemWithAmount[];
    products: ItemWithAmount[];
    iconId: string;
};
