export type Entity = {
    key: string;
    type: string;
    name: string;
    label: string;
    description: string;
    iconId: string;
};
