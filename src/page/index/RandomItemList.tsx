import { useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useLoaderData, useRevalidator } from "react-router-dom";
import { CardGrid, ItemCard } from "../../components/card";
import { Section } from "../../components/section/Section";
import { SectionHead } from "../../components/section/SectionHead";
import { IndexPageData } from "./IndexPage";

export const RandomItemList = () => {
    const { t } = useTranslation();
    const data = useLoaderData() as IndexPageData;
    const randomItems = data.randomItems;
    const revalidator = useRevalidator();

    const shuffleClick = useCallback(() => {
        revalidator.revalidate();
    }, [revalidator]);

    return (
        <Section>
            <SectionHead label={t("page.index.random-item-list.headline")} />
            <button onClick={shuffleClick}>Refresh Random Items</button>
            <CardGrid>
                {randomItems.map((item) => (
                    <ItemCard key={item.key} item={item} />
                ))}
            </CardGrid>
        </Section>
    );
};
