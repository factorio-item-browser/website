import { useDocumentTitle } from "../../hooks/document";
import { Item, Mod } from "../../model/entity";
import { repository } from "../../model/repository";
import { ModList } from "./ModList";
import { RandomItemList } from "./RandomItemList";

export type IndexPageData = {
    randomItems: Item[];
    mods: Mod[];
};

export const indexPageLoader = async (): Promise<IndexPageData> => {
    const [randomItems, mods] = await Promise.all([repository.items.findRandom(12), repository.mods.findAll()]);

    return {
        randomItems: randomItems,
        mods: mods,
    };
};

export const IndexPage = () => {
    useDocumentTitle();

    return (
        <>
            <RandomItemList />
            <ModList />
        </>
    );
};
