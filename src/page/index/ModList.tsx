import { useTranslation } from "react-i18next";
import { useLoaderData } from "react-router-dom";
import { CardGrid, ModCard } from "../../components/card";
import { Section } from "../../components/section/Section";
import { SectionHead } from "../../components/section/SectionHead";
import { IndexPageData } from "./IndexPage";

export const ModList = () => {
    const { t } = useTranslation();
    const data = useLoaderData() as IndexPageData;
    const mods = data.mods;

    return (
        <Section>
            <SectionHead label={t("page.index.mod-list.headline", { count: mods.length })} />
            <CardGrid>
                {mods.map((mod) => (
                    <ModCard key={mod.key} mod={mod} />
                ))}
            </CardGrid>
        </Section>
    );
};
