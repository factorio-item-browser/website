import { mdiPlaylistPlus } from "@mdi/js";
import { useTranslation } from "react-i18next";
import { useLoaderData } from "react-router-dom";
import { Button } from "../../components/button/Button";
import { CardGrid, RecipeCard } from "../../components/card";
import { Section } from "../../components/section/Section";
import { SectionHead } from "../../components/section/SectionHead";
import { usePagination } from "../../hooks/pagination";
import { ItemDetailsData } from "./ItemDetailsPage";

export const IngredientRecipeList = () => {
    const { t } = useTranslation();
    const data = useLoaderData() as ItemDetailsData;
    const allRecipes = data.ingredientRecipes;
    const [recipes, hasNextPage, nextPage] = usePagination(allRecipes, 24);

    if (!recipes || recipes.length === 0) {
        return null;
    }

    return (
        <Section>
            <SectionHead label={t("page.item-details.ingredient-recipe-list.headline", { count: allRecipes.length })} />
            <CardGrid>
                {recipes.map((recipe) => (
                    <RecipeCard key={recipe.key} recipe={recipe} />
                ))}
            </CardGrid>
            {hasNextPage ? (
                <Button
                    primary
                    label={t("page.item-details.show-more-recipes")}
                    iconPath={mdiPlaylistPlus}
                    onClick={nextPage}
                />
            ) : null}
        </Section>
    );
};
