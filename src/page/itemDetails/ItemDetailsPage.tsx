import { useTranslation } from "react-i18next";
import { LoaderFunctionArgs, useLoaderData } from "react-router-dom";
import { ItemTitleCard } from "../../components/card";
import { useDocumentTitle } from "../../hooks/document";
import { useViewedEntity } from "../../hooks/sidebar";
import { Item, ItemType, Recipe } from "../../model/entity";
import { buildKey, repository } from "../../model/repository";
import { IngredientRecipeList } from "./IngredientRecipeList";
import { ProductRecipeList } from "./ProductRecipeList";

export type ItemDetailsData = {
    item: Item;
    productRecipes: Recipe[];
    ingredientRecipes: Recipe[];
};

type Params = {
    name: string;
};

export const itemDetailsLoader =
    (type: ItemType) =>
    async ({ params }: LoaderFunctionArgs & { params: Params }): Promise<ItemDetailsData> => {
        const item = await repository.items.findByKey(buildKey(type, params.name));
        if (!item) {
            return {
                item: {} as Item,
                productRecipes: [],
                ingredientRecipes: [],
            };
        }

        const [productRecipes, ingredientRecipes] = await Promise.all([
            repository.recipes.findWithProduct(item.key),
            repository.recipes.findWithIngredient(item.key),
        ]);

        return {
            item: item,
            productRecipes: productRecipes,
            ingredientRecipes: ingredientRecipes,
        };
    };

export const ItemDetailsPage = () => {
    const { t } = useTranslation();
    const data = useLoaderData() as ItemDetailsData;
    const item = data.item;
    const title = item ? t("page.item-details.title", { type: t(`entity.${item.type}`), name: item.label }) : undefined;

    useDocumentTitle(title);
    useViewedEntity(item);

    if (!item) {
        return null;
    }

    return (
        <>
            <ItemTitleCard item={item} />
            <ProductRecipeList />
            <IngredientRecipeList />
        </>
    );
};
