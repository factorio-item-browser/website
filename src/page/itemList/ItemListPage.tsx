import { useDocumentTitle } from "../../hooks/document";
import { Item } from "../../model/entity";
import { repository } from "../../model/repository";
import { ItemGrid } from "./ItemGrid";

export type ItemListData = {
    items: Item[];
};

export const itemListLoader = async (): Promise<ItemListData> => {
    const items = await repository.items.findAll();

    return {
        items: items,
    };
};

export const ItemListPage = () => {
    useDocumentTitle("page.item-list.title");

    return (
        <>
            <ItemGrid />
        </>
    );
};
