import { useTranslation } from "react-i18next";
import { useLoaderData } from "react-router-dom";
import { IconGridCard } from "../../components/card";
import { Icon, IconSize } from "../../components/icon";
import { ItemLink } from "../../components/link";
import { Section } from "../../components/section/Section";
import { SectionHead } from "../../components/section/SectionHead";
import { ItemListData } from "./ItemListPage";

export const ItemGrid = () => {
    const { t } = useTranslation();
    const data = useLoaderData() as ItemListData;
    const items = data.items;

    return (
        <Section>
            <SectionHead label={t("page.item-list.headline", { count: items.length })} />
            <IconGridCard>
                {items.map((item) => (
                    <ItemLink key={item.key} item={item}>
                        <Icon id={item.iconId} size={IconSize.Large} />
                    </ItemLink>
                ))}
            </IconGridCard>
        </Section>
    );
};
