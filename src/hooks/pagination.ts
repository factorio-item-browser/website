import { useCallback, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";

export function usePagination<T>(items: T[], itemsPerPage: number): [T[], boolean, () => void] {
    const location = useLocation();
    const [page, setPage] = useState<number>(1);

    useEffect(() => {
        setPage(1);
    }, [location.pathname]);

    const nextPage = useCallback(() => {
        setPage(page + 1);
    }, [page]);

    return [items.slice(0, page * itemsPerPage), page * itemsPerPage < items.length, nextPage];
}
