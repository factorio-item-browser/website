import { useEffect, useMemo } from "react";
import { useRecoilState } from "recoil";
import { Entity } from "../model/entity";
import { sidebarLastViewedState } from "../state/sidebar";

function addLastViewed(lastViewed: Entity[], entity: Entity | null): Entity[] {
    console.log("addLastViewed", lastViewed, entity);
    if (!entity) {
        return lastViewed;
    }

    const newLastViewed = lastViewed.filter((e) => e.key !== entity.key);
    console.log(newLastViewed);

    return [entity, ...newLastViewed].slice(0, 10);
}

export function useViewedEntity(entity: Entity | null) {
    const [lastViewed, setLastViewed] = useRecoilState(sidebarLastViewedState);

    /* eslint-disable react-hooks/exhaustive-deps */
    const newLastViewed = useMemo(() => {
        return addLastViewed(lastViewed, entity);
    }, [lastViewed.map((entity) => entity.key).join("|"), entity?.key]);

    /* eslint-disable react-hooks/exhaustive-deps */
    useEffect(() => {
        setLastViewed(newLastViewed);
    }, [newLastViewed.map((entity) => entity.key).join("|")]);
}
