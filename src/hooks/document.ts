import { useEffect } from "react";
import { useTranslation } from "react-i18next";

export function useDocumentTitle(titleKey?: string, options?: { [key: string]: unknown }): void {
    const { t } = useTranslation();

    useEffect(() => {
        if (titleKey) {
            const translatedTitle = t(titleKey, { ...options });
            document.title = t("page.title.extended", { title: translatedTitle });
        } else {
            document.title = t("page.title.default");
        }
    }, [t, titleKey, options]);
}
