import { useRef } from "react";
import { RecoilValue, useRecoilValueLoadable } from "recoil";

export function useRecoilValueLazy<T>(recoilValue: RecoilValue<T>): T | undefined {
    const value = useRecoilValueLoadable(recoilValue);
    const ref = useRef<typeof value>();

    if (value.state === "hasValue") {
        ref.current = value;
        return value.contents;
    }

    if (value.state === "loading" && ref.current?.state === "hasValue") {
        return ref.current?.contents;
    }

    if (value.state === "hasError") {
        throw value.contents;
    }

    return undefined;
}
