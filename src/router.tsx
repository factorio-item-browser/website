import { createBrowserRouter, LoaderFunction } from "react-router-dom";
import { Layout } from "./components/layout";
import { ItemType } from "./model/entity";
import { IndexPage, indexPageLoader } from "./page/index";
import { itemDetailsLoader, ItemDetailsPage } from "./page/itemDetails";
import { itemListLoader, ItemListPage } from "./page/itemList";

export const router = createBrowserRouter([
    {
        path: "/",
        element: <Layout />,
        children: [
            {
                path: "",
                element: <IndexPage />,
                loader: indexPageLoader,
            },
            {
                path: "items",
                element: <ItemListPage />,
                loader: itemListLoader,
            },
            {
                path: "fluid/:name",
                element: <ItemDetailsPage />,
                loader: itemDetailsLoader(ItemType.Fluid) as LoaderFunction,
            },
            {
                path: "item/:name",
                element: <ItemDetailsPage />,
                loader: itemDetailsLoader(ItemType.Item) as LoaderFunction,
            },
            {
                path: "resource/:name",
                element: <ItemDetailsPage />,
                loader: itemDetailsLoader(ItemType.Resource) as LoaderFunction,
            },
        ],
    },
]);
