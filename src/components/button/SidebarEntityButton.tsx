import classNames from "classnames";
import { useTranslation } from "react-i18next";
import { Entity } from "../../model/entity";
import { Icon, FavoriteIcon } from "../icon";
import { Link } from "../link";
import { Text, TextSize, TextStyle } from "../text";
import styles from "./SidebarEntityButton.module.css";

type Props = {
    /** The entity to display the button for. */
    entity: Entity;
    /** Whether the entity is currently in the list of favorites. */
    favorite?: boolean;
    /** The additional className to attach. */
    className?: string;
};

export const SidebarEntityButton = ({ entity, favorite, className }: Props) => {
    const { t } = useTranslation();
    const classes = classNames(className, styles.button);

    const route = `/${entity.type}/${entity.name}`;

    return (
        <Link className={classes} route={route}>
            <Icon className={styles.icon} id={entity.iconId} />
            <Text
                className={styles.type}
                text={t(`entity.${entity.type}`)}
                size={TextSize.Small}
                style={TextStyle.Uppercase}
                nowrap
            />
            <Text className={styles.label} text={entity.label} nowrap />
            <FavoriteIcon className={styles.star} active={favorite} />
        </Link>
    );
};
