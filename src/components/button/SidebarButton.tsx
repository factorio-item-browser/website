import Icon from "@mdi/react";
import classNames from "classnames";
import { Link } from "../link";
import { Text } from "../text";
import styles from "./SidebarButton.module.css";

type Props = {
    label: string;
    route: string;
    icon: string;
    primary?: boolean;
    /** The additional className to attach. */
    className?: string;
};

export const SidebarButton = ({ label, route, icon, primary, className }: Props) => {
    const classes = classNames(className, styles.button);

    return (
        <Link className={classes} route={route}>
            <Icon className={styles.icon} path={icon} />
            <Text className={styles.label} text={label} />
        </Link>
    );
};
