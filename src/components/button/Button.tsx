import classNames from "classnames";
import { Icon } from "../icon";
import styles from "./Button.module.css";

type Props = {
    /** The label to display in the button. */
    label: string;
    /** Whether the button is a primary one. */
    primary?: boolean;
    /** The id of the stylesheet icon to display on the button. */
    iconId?: string;
    /** The path of the Material Design icon to display on the button. */
    iconPath?: string;
    /** The handler to call on a click. */
    onClick?: () => void | Promise<void>;
    /** The additional className to attach. */
    className?: string;
};

export const Button = ({ label, primary, iconId, iconPath, onClick, className }: Props) => {
    const classes = classNames(className, styles.button, {
        [styles.primary]: primary,
    });

    let icon = null;
    if (iconId || iconPath) {
        icon = <Icon className={styles.icon} id={iconId} path={iconPath} />;
    }

    return (
        <div className={classes} onClick={onClick}>
            {icon}
            <span className={styles.label}>{label}</span>
        </div>
    );
};
