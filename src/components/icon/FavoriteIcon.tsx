import { mdiStar, mdiStarMinus, mdiStarOutline, mdiStarPlusOutline } from "@mdi/js";
import classNames from "classnames";
import { useCallback, useState, MouseEvent, useMemo } from "react";
import styles from "./FavoriteIcon.module.css";
import { Icon } from "./Icon";

type Props = {
    /** Whether the favorite is active, i.e. the click action is removing it from the favorites. */
    active?: boolean;
    /** The handler for when the user clicked on the star. */
    onClick?: () => void | Promise<void>;
    /** The additional className to attach. */
    className?: string;
};

export const FavoriteIcon = ({ active, onClick, className }: Props) => {
    const [hover, setHover] = useState(false);
    const handleMouseEnter = useCallback(() => {
        setHover(true);
    }, [setHover]);
    const handleMouseLeave = useCallback(() => {
        setHover(false);
    }, [setHover]);
    const handleClick = useCallback(
        (event: MouseEvent<HTMLDivElement>) => {
            event.preventDefault();
            event.stopPropagation();
            if (onClick) {
                onClick();
            }
        },
        [onClick],
    );

    const iconPath = useMemo(() => {
        if (active) {
            return hover ? mdiStarMinus : mdiStar;
        }

        return hover ? mdiStarPlusOutline : mdiStarOutline;
    }, [active, hover]);

    const classes = classNames(className, styles.favorite);

    return (
        <div
            className={classes}
            onMouseEnter={handleMouseEnter}
            onMouseLeave={handleMouseLeave}
            // title={"Add to favorites bla blubb"}
            onClick={handleClick}
        >
            <Icon path={iconPath} />
        </div>
    );
};
