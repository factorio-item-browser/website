import { mdiChevronRight } from "@mdi/js";
import classNames from "classnames";
import { Recipe } from "../../model/entity";
import { AttachedText } from "../text";
import { Icon, IconSize } from "./Icon";
import styles from "./RecipeSeparatorIcon.module.css";

type Props = {
    /** The recipe to display the separator icon for. */
    recipe: Recipe;
    /** The additional className to attach. */
    className?: string;
};

export const RecipeSeparatorIcon = ({ recipe, className }: Props) => {
    const time = formatCraftingTime(recipe.time);
    const classes = classNames(className, styles.separator, {
        [styles.infinity]: time === "∞",
    });

    return (
        <AttachedText className={classes} text={time} bottom>
            <Icon className={styles.icon} path={mdiChevronRight} size={IconSize.Large} />
        </AttachedText>
    );
};

function formatCraftingTime(craftingTime: number): string {
    if (craftingTime <= 0) {
        return "";
    }
    if (craftingTime <= 600) {
        return Math.round(craftingTime * 100) / 100 + "s";
    }
    if (craftingTime <= 5400) {
        return Math.round((craftingTime * 10) / 60) / 10 + "min";
    }
    if (craftingTime <= 86400) {
        return Math.round((craftingTime * 10) / 3600) / 10 + "h";
    }

    return "∞";
}
