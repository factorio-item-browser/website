import classNames from "classnames";
import { ItemWithAmount } from "../../model/entity";
import { ItemLink } from "../link";
import { AttachedText } from "../text";
import { Icon } from "./Icon";
import styles from "./ItemIconWithAmount.module.css";

type Props = {
    /** The item to display as icon. */
    item: ItemWithAmount;
    /** The additional className to attach. */
    className?: string;
};

export const ItemIconWithAmount = ({ item, className }: Props) => {
    const classes = classNames(className, styles.icon);

    return (
        <ItemLink item={item} className={classes}>
            <AttachedText text={formatAmount(item.amount)} top left>
                <Icon id={item.iconId} />
            </AttachedText>
        </ItemLink>
    );
};

function formatAmount(amount: number): string {
    if (amount > 1000000) {
        return Math.round(amount / 100000) / 10 + "M";
    }
    if (amount > 1000) {
        return Math.round(amount / 100) / 10 + "k";
    }
    if (amount < 1) {
        return Math.round(amount * 1000) / 10 + "%";
    }
    return Math.round(amount * 10) / 10 + "x";
}
