import MdiIcon from "@mdi/react";
import classNames from "classnames";
import React from "react";
import styles from "./Icon.module.css";

export enum IconSize {
    Small = "small",
    Medium = "medium",
    Large = "large",
    Huge = "huge",
}

type Props = {
    /** The id of the stylesheet icon to render. */
    id?: string;
    /** The path of the Material Design icon to render. */
    path?: string;
    /** The size of the icon. */
    size?: IconSize;
    /** The additional className to attach. */
    className?: string;
};

/** Some documentation. */
export const Icon = ({ id, path, size = IconSize.Medium, className }: Props) => {
    const classes = classNames(className, styles.icon, styles[size], {
        [`icon-${id}`]: !path,
    });

    if (path) {
        return <MdiIcon path={path} className={classes} />;
    }

    return <div className={classes} />;
};
