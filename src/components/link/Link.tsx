import classNames from "classnames";
import { ReactNode, useCallback, MouseEvent } from "react";
import { useNavigate } from "react-router-dom";
import styles from "./Link.module.css";

type Props = {
    /** The route to link to. */
    route: string;
    /** The content of the link. */
    children: ReactNode;
    /** The additional className to attach. */
    className?: string;
};

export const Link = ({ route, children, className }: Props) => {
    const navigate = useNavigate();
    const click = useCallback(
        (event: MouseEvent<HTMLAnchorElement>) => {
            event.preventDefault();
            event.stopPropagation();

            navigate(route);
            return false;
        },
        [route, navigate],
    );

    const classes = classNames(className, styles.link);

    return (
        <a className={classes} href={route} onClick={click}>
            {children}
        </a>
    );
};
