import { ReactNode } from "react";
import { Link } from "./Link";

type Props = {
    /** The contents of the link. */
    children: ReactNode;
    /** The additional className to attach. */
    className?: string;
};

export const IndexLink = ({ children, className }: Props) => {
    return (
        <Link route="/" className={className}>
            {children}
        </Link>
    );
};
