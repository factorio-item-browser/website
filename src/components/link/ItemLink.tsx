import { ReactNode } from "react";
import { Item } from "../../model/entity/item";
import { Link } from "./Link";

type Props = {
    /** The item to link to. */
    item: Item;
    /** The content of the link. */
    children: ReactNode;
    /** The additional className to attach. */
    className?: string;
};

export const ItemLink = ({ item, children, className }: Props) => {
    return (
        <Link className={className} route={`/${item.type}/${item.name}`}>
            {children}
        </Link>
    );
};
