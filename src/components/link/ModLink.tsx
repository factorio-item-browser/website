import { ReactNode } from "react";
import { Mod } from "../../model/entity/mod";
import { ExternalLink } from "./ExternalLink";

type Props = {
    /** The mod to link to. */
    mod: Mod;
    /** The contents of the link. */
    children: ReactNode;
    /** The additional className to attach */
    className?: string;
};

export const ModLink = ({ mod, className, children }: Props) => {
    let link = `https://mods.factorio.com/mod/${mod.name}`;
    if (mod.name === "base") {
        link = "https://www.factorio.com/";
    }

    return (
        <ExternalLink url={link} className={className}>
            {children}
        </ExternalLink>
    );
};
