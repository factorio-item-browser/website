import classNames from "classnames";
import React, { ReactNode } from "react";
import styles from "./ExternalLink.module.css";

type Props = {
    /** The url to link to. */
    url: string;
    /** The content of the link. */
    children: ReactNode;
    /** The additional className to attach. */
    className?: string;
};

export const ExternalLink = ({ url, className, children, ...props }: Props) => {
    const classes = classNames(className, styles.link);

    return (
        <a {...props} className={classes} href={url} target="_blank" rel="noopener noreferrer nofollow">
            {children}
        </a>
    );
};
