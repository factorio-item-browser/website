import classNames from "classnames";
import { ReactNode } from "react";
import styles from "./AttachedText.module.css";
import { Text, TextStyle } from "./Text";

type Props = {
    /** The content to attach the text to. */
    children: ReactNode;
    /** The text to attach. */
    text: string;
    /** Whether to align the attached text to the bottom. */
    bottom?: boolean;
    /** Whether to align the attached text to the top. */
    top?: boolean;
    /** Whether to align the attached text to the left. */
    left?: boolean;
    /** Whether to align the attached text to the right. */
    right?: boolean;
    /** The additional className to attach. */
    className?: string;
};

export const AttachedText = ({
    text,
    children,
    bottom = false,
    top = false,
    left = false,
    right = false,
    className,
}: Props) => {
    const classes = classNames(className, styles.wrapper);
    const textClasses = classNames(styles.text, {
        [styles.top]: top,
        [styles.left]: left,
        [styles.right]: right,
        [styles.bottom]: bottom,
    });

    return (
        <span className={classes}>
            {children}
            <Text className={textClasses} text={text} style={TextStyle.White} />
        </span>
    );
};
