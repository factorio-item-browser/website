import classNames from "classnames";
import styles from "./Headline.module.css";

type Props = {
    /** The label of the headline. */
    label: string;
    /** Whether the headline is only secondary. */
    secondary?: boolean;
    /** The additional className to attach. */
    className?: string;
};

export const Headline = ({ label, secondary, className }: Props) => {
    const classes = classNames(className, styles.headline, {
        [styles.secondary]: secondary,
    });

    if (secondary) {
        return <h3 className={classes}>{label}</h3>;
    }

    return <h2 className={classes}>{label}</h2>;
};
