import classNames from "classnames";
import styles from "./Text.module.css";

export enum TextSize {
    Small = "small",
    Medium = "medium",
    Large = "large",
    Huge = "huge",
}

export enum TextStyle {
    Default = "default",
    Uppercase = "uppercase",
    White = "white",
}

type Props = {
    /** The text to display. */
    text: string;
    /** The size of the text. */
    size?: TextSize;
    /** The style of the text. */
    style?: TextStyle;
    /** Whether the text should not be wrapped, and instead cut off at the end. */
    nowrap?: boolean;
    /** The additional className to attach. */
    className?: string;
};

export const Text = ({ text, size = TextSize.Medium, style = TextStyle.Default, nowrap = false, className }: Props) => {
    const classes = classNames(className, styles.text, styles[`size-${size}`], styles[`style-${style}`], {
        [styles.nowrap]: nowrap,
    });

    return <span className={classes}>{text}</span>;
};
