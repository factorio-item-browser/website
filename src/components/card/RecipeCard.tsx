import { useTranslation } from "react-i18next";
import { Recipe } from "../../model/entity/recipe";
import { Card } from "./Card";
import { CardHead, CardRecipeLine } from "./part";

type Props = {
    /** The recipe to show the card of. */
    recipe: Recipe;
    /** The additional className to attach. */
    className?: string;
};

export const RecipeCard = ({ recipe, className }: Props) => {
    const { t } = useTranslation();

    return (
        <Card className={className}>
            <CardHead
                label={t(`card.recipe-card.label.${recipe.type}`, { name: recipe.label })}
                iconId={recipe.iconId}
            />
            <CardRecipeLine recipe={recipe} />
        </Card>
    );
};
