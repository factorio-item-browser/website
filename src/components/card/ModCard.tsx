import { mdiOpenInNew } from "@mdi/js";
import { useTranslation } from "react-i18next";
import { Mod } from "../../model/entity/mod";
import { Icon, IconSize } from "../icon";
import { ModLink } from "../link";
import { Text, TextSize } from "../text";
import { Card } from "./Card";
import styles from "./ModCard.module.css";
import { CardLine } from "./part";

type Props = {
    /** The mod to be displayed. */
    mod: Mod;
    /** The additional className to attach. */
    className?: string;
};

export const ModCard = ({ mod, className }: Props) => {
    const { t } = useTranslation();

    return (
        <ModLink mod={mod} className={className}>
            <Card className={styles.card}>
                <Icon className={styles.icon} id={mod.iconId} size={IconSize.Huge} />
                <Text text={mod.label} size={TextSize.Large} className={styles.label} nowrap />
                <CardLine className={styles.line} />
                <div className={styles.data}>
                    <dl>
                        <dt>{t("card.mod-card.name")}</dt>
                        <dd>{mod.name}</dd>
                        <dt>{t("card.mod-card.author")}</dt>
                        <dd>{mod.author}</dd>
                        <dt>{t("card.mod-card.version")}</dt>
                        <dd>{mod.version}</dd>
                    </dl>
                </div>
                <Icon className={styles.external} path={mdiOpenInNew} size={IconSize.Small} />
            </Card>
        </ModLink>
    );
};
