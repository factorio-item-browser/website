import classNames from "classnames";
import { ReactNode } from "react";
import styles from "./CardGrid.module.css";

type Props = {
    /** The elements to be arranged in a grid. */
    children: ReactNode;
    /** The additional className to attach. */
    className?: string;
};

export const CardGrid = ({ className, children }: Props) => {
    const classes = classNames(className, styles.grid);

    return <div className={classes}>{children}</div>;
};
