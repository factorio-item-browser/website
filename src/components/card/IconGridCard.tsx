import classNames from "classnames";
import { ReactNode } from "react";
import { Card } from "./Card";
import styles from "./IconGridCard.module.css";

type Props = {
    /** The icons to display in a grid. */
    children: ReactNode;
    /** The additional className to attach. */
    className?: string;
};

export const IconGridCard = ({ className, children }: Props) => {
    const classes = classNames(className, styles.grid);

    return <Card className={classes}>{children}</Card>;
};
