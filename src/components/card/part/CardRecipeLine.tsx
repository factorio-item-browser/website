import classNames from "classnames";
import { Recipe } from "../../../model/entity";
import { ItemIconWithAmount, RecipeSeparatorIcon } from "../../icon";
import styles from "./CardRecipeLine.module.css";

type Props = {
    /** The recipe to display in the line. */
    recipe: Recipe;
    /** The additional className to attach. */
    className?: string;
};

export const CardRecipeLine = ({ recipe, className }: Props) => {
    const classes = classNames(className, styles.line);

    return (
        <div className={classes}>
            <div className={styles.ingredients}>
                {recipe.ingredients.map((ingredient) => (
                    <ItemIconWithAmount key={ingredient.key} item={ingredient} className={styles.icon} />
                ))}
            </div>
            <RecipeSeparatorIcon className={styles.separator} recipe={recipe} />
            <div className={styles.products}>
                {recipe.products.map((product) => (
                    <ItemIconWithAmount key={product.key} item={product} className={styles.icon} />
                ))}
            </div>
        </div>
    );
};
