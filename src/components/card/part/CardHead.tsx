import classNames from "classnames";
import { Icon, IconSize } from "../../icon";
import { Text, TextSize } from "../../text";
import styles from "./CardHead.module.css";

type Props = {
    /** The label of the card head. */
    label: string;
    /** The id of the stylesheet icon to display left of the label. */
    iconId?: string;
    /** The path of the Material Design icon to display left of the label. */
    iconPath?: string;
    /** Whether the CardHead is a title to be rendered more prominent. */
    title?: boolean;
    /** The additional className to attach. */
    className?: string;
};

export const CardHead = ({ label, iconId, iconPath, title = false, className }: Props) => {
    const classes = classNames(className, styles.head, { [styles.title]: title });

    return (
        <div className={classes}>
            <Icon id={iconId} path={iconPath} className={styles.icon} size={title ? IconSize.Large : IconSize.Medium} />
            <Text text={label} size={title ? TextSize.Huge : TextSize.Large} className={styles.label} nowrap={!title} />
        </div>
    );
};
