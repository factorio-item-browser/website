import { Item } from "../../model/entity/item";
import { ItemLink } from "../link";
import { Card } from "./Card";
import { CardHead } from "./part";

type Props = {
    item: Item;
    className?: string;
};

export const ItemCard = ({ item, className }: Props) => {
    return (
        <ItemLink item={item} className={className}>
            <Card>
                <CardHead label={item.label} iconId={item.iconId} />
            </Card>
        </ItemLink>
    );
};
