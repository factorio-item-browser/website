import { ReactNode } from "react";
import { useTranslation } from "react-i18next";
import { Item, ItemType } from "../../model/entity";
import { Card } from "./Card";
import { CardCopyTemplate, CardHead, CardLine, CardText } from "./part";

type Props = {
    /** The item to display the title card of. */
    item: Item;
    /** The additional className to attach. */
    className?: string;
};

export const ItemTitleCard = ({ item, className }: Props) => {
    const { t } = useTranslation();
    const elements: ReactNode[] = [];

    if (item.description) {
        elements.push(<CardText key={"description"} text={item.description} />);
    }

    elements.push(
        <CardCopyTemplate
            key={"copy-template-icon"}
            label={t("card.item-title-card.copy-template-icon-label")}
            value={`[${item.type}=${item.name}]`}
            description={t("card.item-title-card.copy-template-icon-description")}
        />,
    );

    if (item.type === ItemType.Item) {
        elements.push(
            <CardCopyTemplate
                key={"copy-template-cheat"}
                label={t("card.item-title-card.copy-template-cheat-label")}
                value={`/c game.player.insert{ name="${item.name}", count=10 }`}
                description={t("card.item-title-card.copy-template-cheat-description")}
            />,
        );
    }

    return (
        <Card className={className}>
            <CardHead
                label={t(`card.item-title-card.label.${item.type}`, { name: item.label })}
                iconId={item.iconId}
                title
            />
            {elements.reduce((prev, curr, idx) => [prev, <CardLine key={idx} />, curr])}
        </Card>
    );
};
