import classNames from "classnames";
import { ReactNode } from "react";
import styles from "./Card.module.css";

type Props = {
    className?: string;
    children: ReactNode;
};

export const Card = ({ className, children }: Props) => {
    const classes = classNames(className, styles.card);

    return <div className={classes}>{children}</div>;
};
