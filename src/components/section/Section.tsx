import { ReactNode } from "react";

type Props = {
    /** The content of the section. */
    children: ReactNode;
    /** The additional className to attach. */
    className?: string;
};

export const Section = ({ className, children }: Props) => {
    return <section className={className}>{children}</section>;
};
