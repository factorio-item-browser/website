import classNames from "classnames";
import styles from "./SectionHead.module.css";

type Props = {
    /** The label of the section head. */
    label: string;
    /** The additional className to attach. */
    className?: string;
};

export const SectionHead = ({ label, className }: Props) => {
    const classes = classNames(className, styles.head);

    return <h2 className={classes}>{label}</h2>;
};
