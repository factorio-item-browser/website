import { mdiViewComfy } from "@mdi/js";
import classNames from "classnames";
import { ChangeEvent, useCallback } from "react";
import { useRecoilState, useRecoilValue } from "recoil";
import { availableLanguages } from "../../../locales";
import { RecipeMode } from "../../../model/entity";
import { repository } from "../../../model/repository";
import { optionsState } from "../../../state/options";
import { sidebarLastViewedState } from "../../../state/sidebar";
import { Button } from "../../button/Button";
import { SidebarButton } from "../../button/SidebarButton";
import { SidebarEntityButton } from "../../button/SidebarEntityButton";
import { Headline } from "../../text";
import styles from "./Sidebar.module.css";

type Props = {
    /** The additional className to attach. */
    className?: string;
};

export const Sidebar = ({ className }: Props) => {
    const locales = availableLanguages;
    const [options, setOptions] = useRecoilState(optionsState);

    const lastViewedEntities = useRecoilValue(sidebarLastViewedState);

    const handleChangeLocale = useCallback(
        (event: ChangeEvent<HTMLSelectElement>) => {
            const newOptions = {
                ...options,
                locale: event.target.value,
            };
            setOptions(newOptions);
        },
        [options, setOptions],
    );

    const handleChangeRecipeMode = useCallback(
        (event: ChangeEvent<HTMLSelectElement>) => {
            const newOptions = {
                ...options,
                recipeMode: event.target.value as RecipeMode,
            };
            setOptions(newOptions);
        },
        [options, setOptions],
    );

    const classes = classNames(className, styles.sidebar);

    return (
        <aside className={classes}>
            <SidebarButton label={"All items"} route={"/items"} icon={mdiViewComfy} />

            <Headline secondary label="Last viewed" />
            {lastViewedEntities.map((entity) => (
                <SidebarEntityButton key={entity.key} entity={entity} />
            ))}

            <Headline secondary label="Some random debug things" />
            <Button
                label="EXPORTER"
                primary
                onClick={async () => {
                    await repository.import();
                }}
            />

            <div>
                <select onChange={handleChangeLocale} value={options.locale}>
                    {Object.entries(locales).map(([locale, name]) => (
                        <option key={locale} value={locale}>
                            {name}
                        </option>
                    ))}
                </select>
            </div>

            <div>
                <select onChange={handleChangeRecipeMode} value={options.recipeMode}>
                    <option value={RecipeMode.Normal}>Normal</option>
                    <option value={RecipeMode.Expensive}>Expensive</option>
                </select>
            </div>
        </aside>
    );
};
