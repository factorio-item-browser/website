import { Outlet } from "react-router-dom";
import { Clouds } from "./Clouds";
import styles from "./Layout.module.css";
import { Footer } from "./footer";
import { Header } from "./header";
import { Sidebar } from "./sidebar";

export const Layout = () => {
    return (
        <div className={styles.container}>
            <Header className={styles.header} />
            <Sidebar className={styles.sidebar} />
            <main className={styles.content}>
                <Outlet />
            </main>
            <Footer className={styles.footer} />
            {/*<Clouds />*/}
        </div>
    );
};
