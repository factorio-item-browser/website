import React from "react";
import { Trans } from "react-i18next";
import { ExternalLink } from "../../link";
import styles from "./Copyright.module.css";

const year = new Date().getFullYear();

export const Copyright = () => {
    return (
        <div className={styles.copyright}>
            © {year} Factorio Item Browser
            <br />
            <Trans i18nKey="footer.copyright">
                Content and images are owned by&nbsp;
                <ExternalLink url="https://www.factorio.com/">Wube Software</ExternalLink> and the&nbsp;
                <ExternalLink url="https://mods.factorio.com/">mod authors</ExternalLink> respectively.
            </Trans>
        </div>
    );
};
