import classNames from "classnames";
import { Copyright } from "./Copyright";
import styles from "./Footer.module.css";

type Props = {
    /** The additional className to attach. */
    className?: string;
};

export const Footer = ({ className }: Props) => {
    const classes = classNames(className, styles.footer);

    return (
        <footer className={classes}>
            <Copyright />
        </footer>
    );
};
