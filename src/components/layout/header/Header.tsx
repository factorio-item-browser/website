import classNames from "classnames";
import styles from "./Header.module.css";
import { HeaderLogo } from "./HeaderLogo";

type Props = {
    /** The additional className to attach. */
    className?: string;
};

export const Header = ({ className }: Props) => {
    const classes = classNames(className, styles.header);

    return (
        <header className={classes}>
            <HeaderLogo className={styles.logo} />
            <div className={styles.search}>Search</div>
        </header>
    );
};
