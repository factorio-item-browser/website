import logo from "../../../assets/logo.webp";
import { IndexLink } from "../../link";
import styles from "./HeaderLogo.module.css";

type Props = {
    /** The additional className to attach. */
    className?: string;
};

export const HeaderLogo = ({ className }: Props) => {
    return (
        <IndexLink className={className}>
            <img className={styles.logo} src={logo} alt="Factorio Item Browser" />
        </IndexLink>
    );
};
