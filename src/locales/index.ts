import { use } from "i18next";
import { initReactI18next } from "react-i18next";
import localeEn from "./en.json";
import localeMeta from "./meta.json";

(async (): Promise<void> => {
    await use(initReactI18next).init({
        resources: {
            en: {
                translation: localeEn,
            },
        },
        // detection: {
        //     order: ["localStorage", "navigator"],
        //     lookupLocalStorage: "locale",
        //     caches: ["localStorage"],
        // },
        lng: "en",
        fallbackLng: "en",
        interpolation: {
            escapeValue: false,
        },
        supportedLngs: Object.keys(localeMeta),
    });
})();

export const availableLanguages = localeMeta;
