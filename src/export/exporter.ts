import { Buffer } from "buffer";
import { ExportItem, ExportMachine, ExportMod, ExportRecipe } from "./data";

type Listener<T> = (t: T) => void | Promise<void>;
const noop = () => {
    // Do noting
};

export class Exporter {
    public onEnd: Listener<void> = noop;
    public onItem: Listener<ExportItem> = noop;
    public onMachine: Listener<ExportMachine> = noop;
    public onMod: Listener<ExportMod> = noop;
    public onRecipe: Listener<ExportRecipe> = noop;

    public async process(name: string): Promise<void> {
        const response = await fetch(`https://www.factorio-item-browser.com/amadeus/${name}.jsonl`);
        if (!response.body) {
            return;
        }

        let buffer = "";

        const reader = response.body.getReader();
        while (true) {
            const { done, value } = await reader.read();
            if (value) {
                buffer += Buffer.from(value).toString();

                const lines = buffer.split("\n");
                buffer = lines.pop() || "";

                for (const line of lines) {
                    await this.processLine(line);
                }
            }

            if (done) {
                this.onEnd();
                break;
            }
        }
    }

    private async processLine(line: string) {
        const data = JSON.parse(line);
        switch (data.type) {
            case "fluid":
            case "item":
            case "resource":
                await this.onItem(data as ExportItem);
                break;

            case "machine":
                await this.onMachine(data as ExportMachine);
                break;

            case "mod":
                await this.onMod(data as ExportMod);
                break;

            case "mining":
            case "recipe":
            case "rocket-launch":
                await this.onRecipe(data as ExportRecipe);
                break;
        }
    }
}
