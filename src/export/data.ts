export type ExportTranslations = Record<string, string>;

export type ExportItem = {
    type: string;
    name: string;
    labels: ExportTranslations;
    descriptions: ExportTranslations;
    icon: string;
};

export type ExportMachine = {
    type: string;
    name: string;
    labels: ExportTranslations;
    descriptions: ExportTranslations;
    icon: string;
    craftingCategories: string[];
    resourceCategories: string[];
    speed: number;
    numberOfItemSlots: number;
    numberOfFluidInputSlots: number;
    numberOfFluidOutputSlots: number;
    numberOfModuleSlots: number;
    energyUsage: number;
    energyUsageUnit: string;
};

export type ExportMod = {
    type: string;
    name: string;
    labels: ExportTranslations;
    descriptions: ExportTranslations;
    icon: string;
    author: string;
    version: string;
};

export type ExportRecipe = {
    type: string;
    name: string;
    mode: string;
    labels: ExportTranslations;
    descriptions: ExportTranslations;
    icon: string;
    time: number;
    category: string;
    ingredients: ExportRecipeIngredient[];
    products: ExportRecipeProduct[];
};

export type ExportRecipeIngredient = {
    type: string;
    name: string;
    amount: number;
};

export type ExportRecipeProduct = {
    type: string;
    name: string;
    amountMin: number;
    amountMax: number;
    probability: number;
};
